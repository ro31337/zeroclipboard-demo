/*!
 * GitHub style ZeroClipboard tooltips
 *
 * Requires
 *   jQuery: http://jquery.com
 *   ZeroClipboard: http://zeroclipboard.github.io/ZeroClipboard/
 *   tipsy: http://onehackoranother.com/projects/jquery/tipsy/
 *
 * Example
 *   <button class="copy-button" data-clipboard-text="Copy Me!">Copy to Clipboard</button>
 *   <button class="copy-button" title="copy to clipboard" data-copied-hint="copied!" data-clipboard-text="Copy Me!">Copy to Clipboard</button>
 */
$(function() {
  if ($('.copy-button').length) {
    var _defaults = {
      title: 'copy to clipboard',
      copied_hint: 'copied!',
      gravity: $.fn.tipsy.autoNS
    };

    var clip = new ZeroClipboard($('.copy-button'), {
      moviePath: '/ZeroClipboard.swf'
    });

    clip.on('load', function(client) {
      $(clip.htmlBridge).tipsy({ gravity: _defaults.gravity });
      $(clip.htmlBridge).attr('title', _defaults.title);
    });

    clip.on('complete', function(client, args) {
      var copied_hint = $(this).data('copied-hint');
      if (!copied_hint) {
        copied_hint = _defaults.copied_hint;
      }

      $(clip.htmlBridge)
        .prop('title', copied_hint)
        .tipsy('show');
    });

    clip.on('noflash wrongflash', function(client) {
      $('.copy-button').hide();
    });
  }
});
